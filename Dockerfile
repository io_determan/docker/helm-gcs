FROM google/cloud-sdk:216.0.0-alpine

LABEL maintainer="Mark Determan <mark@determan.io>"

ARG VCS_REF
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://github.com/mdeterman/helm-gcs" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="/Dockerfile"

ENV HELM_LATEST_VERSION="v2.10.0"
ENV HELM_GCS_LATEST_VERSION="v0.2.0"

RUN apk add --update ca-certificates \
 && apk add --update -t deps wget \
 && wget https://storage.googleapis.com/kubernetes-helm/helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz \
 && tar -xvf helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz \
 && mv linux-amd64/helm /usr/local/bin \
 && apk del --purge deps \
 && rm /var/cache/apk/* \
 && rm -f /helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz

RUN helm init --client-only
RUN helm plugin install https://github.com/viglesiasce/helm-gcs.git --version ${HELM_GCS_LATEST_VERSION}

ENTRYPOINT ["helm"]
CMD ["help"]